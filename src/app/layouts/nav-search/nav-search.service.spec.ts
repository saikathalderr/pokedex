import { TestBed } from '@angular/core/testing';

import { NavSearchService } from './nav-search.service';

describe('NavSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavSearchService = TestBed.get(NavSearchService);
    expect(service).toBeTruthy();
  });
});
