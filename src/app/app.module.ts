import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavBarComponent } from "./layouts/nav-bar/nav-bar.component";
import { NavSearchComponent } from "./layouts/nav-search/nav-search.component";
import { PokemonListingPageComponent } from "./components/pokemon-listing-page/pokemon-listing-page.component";
import { PokemonCardComponent } from "./components/pokemon-card/pokemon-card.component";
import { PokemonInfoComponent } from "./components/pokemon-info/pokemon-info.component";
import { BackButtonComponent } from "./layouts/back-button/back-button.component";

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    NavSearchComponent,
    PokemonListingPageComponent,
    PokemonCardComponent,
    PokemonInfoComponent,
    BackButtonComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
