import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class PokemonListingService {
  constructor(private httpClient: HttpClient) {}

  getPokemonLists() {
    return this.httpClient.get(`https://pokeapi.co/api/v2/pokemon/?limit=30`);
  }

  getPokemon(pokeUrl) {
    return this.httpClient.get(pokeUrl);
  }
}
