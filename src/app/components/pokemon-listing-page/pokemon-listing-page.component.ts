import { Component, OnInit } from "@angular/core";
import { PokemonListingService } from "./pokemon-listing.service";

@Component({
  selector: "app-pokemon-listing-page",
  templateUrl: "./pokemon-listing-page.component.html",
  styleUrls: ["./pokemon-listing-page.component.css"]
})
export class PokemonListingPageComponent implements OnInit {
  pokemonCounts: any;
  allPokemons: any = [];
  allPokemonsLoaded: boolean;
  constructor(public pokemonListingService: PokemonListingService) {}

  ngOnInit() {
    this.getAllPokemons();
  }

  getAllPokemons() {
    if (this.pokemonCounts == null) {
      this.pokemonListingService.getPokemonLists().subscribe(res => {
        this.pokemonCounts = res;
        this.pokemonCounts.results.forEach(pokemon => {
          this.pokemonListingService.getPokemon(pokemon.url).subscribe(res => {
            this.allPokemons.push(res);
          });
        });
        this.allPokemonsLoaded = true;
      });
    }
  }
}
