import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonListingPageComponent } from './pokemon-listing-page.component';

describe('PokemonListingPageComponent', () => {
  let component: PokemonListingPageComponent;
  let fixture: ComponentFixture<PokemonListingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonListingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonListingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
