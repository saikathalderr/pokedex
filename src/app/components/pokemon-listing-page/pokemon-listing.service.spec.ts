import { TestBed } from '@angular/core/testing';

import { PokemonListingService } from './pokemon-listing.service';

describe('PokemonListingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PokemonListingService = TestBed.get(PokemonListingService);
    expect(service).toBeTruthy();
  });
});
