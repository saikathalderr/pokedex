import { TestBed } from '@angular/core/testing';

import { PokemonInfoService } from './pokemon-info.service';

describe('PokemonInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PokemonInfoService = TestBed.get(PokemonInfoService);
    expect(service).toBeTruthy();
  });
});
