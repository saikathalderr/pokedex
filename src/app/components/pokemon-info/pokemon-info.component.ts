import { Component, OnInit } from "@angular/core";
import { PokemonInfoService } from "./pokemon-info.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-pokemon-info",
  templateUrl: "./pokemon-info.component.html",
  styleUrls: ["./pokemon-info.component.css"]
})
export class PokemonInfoComponent implements OnInit {
  pokemonInfo: Object;
  pokeId: number;

  constructor(
    private pokemonInfoService: PokemonInfoService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getParams();
  }

  getPokeInfo(id) {
    this.pokemonInfoService.getPokemonInfo(id).subscribe(res => {
      this.pokemonInfo = res;
    });
  }

  getParams() {
    this.route.params.subscribe(params => {
      this.pokeId = +params["id"];
      this.getPokeInfo(this.pokeId);
    });
  }
}
