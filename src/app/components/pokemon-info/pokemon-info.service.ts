import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class PokemonInfoService {
  constructor(private http: HttpClient) {}

  getPokemonInfo(pokemonID) {
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${pokemonID}`);
  }
}
