import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PokemonListingPageComponent } from "./components/pokemon-listing-page/pokemon-listing-page.component";
import { PokemonInfoComponent } from "./components/pokemon-info/pokemon-info.component";

const routes: Routes = [
  {
    path: "",
    component: PokemonListingPageComponent
  },
  {
    path: "pokemon/:name/:id",
    component: PokemonInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
